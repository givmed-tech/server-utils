module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  plugins: [
    '@typescript-eslint',
    'svelte3'
  ],
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3'
    }
  ],
  parserOptions: {
    project: './tsconfig.eslint.json',
    tsconfigRootDir: '.',
  },
  rules: {
    'no-console': [
      'error',
      {
        allow: [
          'warn',
          'error',
          'info',
          'time',
          'timeEnd',
          'dir'
        ]
      }
    ],
    indent: [
      'error',
      4
    ],
    quotes: [
      'error',
      'single'
    ],
    'object-curly-spacing': [
      'error',
      'always'
    ],
    semi: [
      'error',
      'always'
    ],
    'eol-last': [2],
    'no-trailing-spaces': [2],
    '@typescript-eslint/no-var-requires': [
      'off'
    ],
    '@typescript-eslint/indent': [
      'error',
      4
    ],
    '@typescript-eslint/no-unused-vars': [
      'off'
    ],
    '@typescript-eslint/explicit-member-accessibility': [
      'off'
    ],
    '@typescript-eslint/explicit-function-return-type': [
      'off'
    ],
    '@typescript-eslint/no-use-before-define': [
      'error', { functions: false }
    ],
    '@typescript-eslint/camelcase': ['off'],
  }
}
