export default {
    files: [
        'src/tests/**/*.test.ts',
    ],
    failFast: true,
    failWithoutAssertions: false,
    verbose: true,
    timeout: '40s',
    typescript: {
        extensions: [
            'ts'
        ],
        rewritePaths: {
            'src/tests/': 'dist/tests/'
        },
        compile: 'tsc',
    },
    require: [
        '@babel/register',
    ],
};
