import { FastifyInstance, FastifyServerOptions } from 'fastify';
import {
    saveTaskInDB,
    deleteTaskById,
    deleteTaskByUniqueExternalId,
    getTaskById,
    getTasks,
    getTaskByUniqueExternalId
} from '../utils/db.util';
import {
    scheduleHttpRequestPostBody,
    scheduleHttpRequestDeleteBody,
    scheduleHttpRequestGetOneReponse,
    scheduleHttpRequestGetAllReponse
} from '../schemas';
import { scheduleTask, getTimestampFromMs, apiKeyValidator } from '../utils/core.util';

export default function attachScheduleHttpTaskRoutes(server: FastifyInstance, opts: FastifyServerOptions, done: () => void): void {
    server.route<{ Querystring: { uniqueExternalId?: string } }>({
        method: 'GET',
        url: '/http-request',
        schema: {
            querystring: {
                type: 'object',
                properties: {
                    uniqueExternalId: { type: ['string', 'null'] },
                },
            },
            response: {
                200: scheduleHttpRequestGetAllReponse
            },
        },
        preValidation: apiKeyValidator(server),
        handler: async (request, reply) => {
            let tasks = [];

            if (request.query.uniqueExternalId) {
                const task = await getTaskByUniqueExternalId(request.query.uniqueExternalId);

                if (task) {
                    tasks.push(task);
                }
            } else {
                tasks = await getTasks();
            }

            reply.send(tasks);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'GET',
        url: '/http-request/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                }
            },
            response: {
                200: scheduleHttpRequestGetOneReponse
            },
        },
        preValidation: apiKeyValidator(server),
        handler: async (request, reply) => {
            const task = await getTaskById(request.params.id);

            if (!task) {
                throw server.httpErrors.notFound();
            }

            reply.send(task);
        }
    });

    server.route<{ Body: ServerUtils.ScheduleHttpRequestPostData }>({
        method: 'POST',
        url: '/http-request',
        schema: {
            body: scheduleHttpRequestPostBody,
            response: {
                200: {
                    type: 'object',
                    properties: {
                        taskId: { type: 'number' },
                        uniqueExternalId: { type: ['string', 'null'] }
                    }
                }
            }
        },
        preValidation: apiKeyValidator(server),
        handler: async (request, reply) => {
            const timestamp = request.body.timestamp || getTimestampFromMs(request.body.ms);
            let task = [];

            if (!timestamp) {
                throw server.httpErrors.badRequest('ms or timestap is invalid');
            }

            try {
                task = await saveTaskInDB(request.body, server.env);
            } catch (error) {
                server.assert.notEqual(error.code, 'SQLITE_CONSTRAINT', 400, error.message);
            }

            if (task.length > 0) {
                scheduleTask({
                    taskId: task[0],
                    requestMethod: request.body.requestMethod,
                    requestUrl: request.body.requestUrl,
                    requestParams: request.body.requestParams,
                    requestData: request.body.requestData,
                    uniqueExternalId: request.body.uniqueExternalId,
                    timestamp,
                    apiKey: server.env.API_KEY
                });

                return reply.code(200).send({
                    taskId: task[0],
                    uniqueExternalId: request.body.uniqueExternalId
                });
            } else {
                throw server.httpErrors.internalServerError('Failed to insert task into sqlite');
            }
        }
    });

    server.route<{ Body: ServerUtils.ScheduleHttpRequestDeleteData }>({
        method: 'DELETE',
        url: '/http-request',
        schema: {
            body: scheduleHttpRequestDeleteBody,
            response: {
                204: {
                    type: 'null'
                }
            }
        },
        preValidation: apiKeyValidator(server),
        handler: async (request, reply) => {
            const { id, uniqueExternalId } = request.body;
            let taskDeleted: number;

            if (!id && !uniqueExternalId) {
                throw server.httpErrors.badRequest('You need to provide id or uniqueExternalId in body');
            } else if (id) {
                taskDeleted = await deleteTaskById(id);
            } else {
                taskDeleted = await deleteTaskByUniqueExternalId(uniqueExternalId);
            }

            if (taskDeleted > 0) {
                return reply.code(204).send();
            } else {
                throw server.httpErrors.notFound('Failed to find task to delete');
            }
        }
    });

    done();
}
