import {
    FastifyInstance,
    FastifyRequest,
    FastifyReply,
    FastifyServerOptions,
} from 'fastify';
import {
    scheduleHttpRequestGetOneReponse,
    scheduleHttpRequestGetAllReponse,
    userResponse,
    userRequest,
    shortUrlsResponse,
    shortUrlsRequest,
    minLength6
} from '../schemas';
import {
    getTaskById,
    getTasks,
    deleteTaskById,
    getUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    createUser,
    getShortUrls,
    getShortUrlById,
    deleteShortUrlkById,
    updateShortUrlById
} from '../utils/db.util';
import { loginUser, createShortUrlHandler } from '../utils/core.util';

function isLoggedIn(server: FastifyInstance) {
    return async (request: FastifyRequest, reply: FastifyReply) => {
        if (!request.session.get('user') || !request.session.get('user').id) {
            throw server.httpErrors.unauthorized();
        }
        return;
    };
}

function isAdmin(server: FastifyInstance) {
    return async (request: FastifyRequest, reply: FastifyReply) => {
        if (!request.session.get('user') || !request.session.get('user').isAdmin) {
            throw server.httpErrors.forbidden('You do not have access to view this content');
        }
        return;
    };
}

export default function attachDashboardRoutes(server: FastifyInstance, opts: FastifyServerOptions, done: () => void): void {
    server.route({
        method: 'GET',
        url: '/',
        handler: (request, reply) => {
            reply.sendFile('./index.html');
        }
    });

    server.route<{ Body: { email: string; password: string } }>({
        method: 'POST',
        url: '/api/login',
        schema: {
            body: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string',
                        minLength: minLength6,
                        errorMessage: {
                            minLength: `Email has to be at least ${minLength6} characters long`
                        }
                    },
                    password: {
                        type: 'string',
                        minLength: minLength6,
                        errorMessage: {
                            minLength: `Password has to be at least ${minLength6} characters long`
                        }
                    }
                },
                required: ['email', 'password'],
                errorMessage: {
                    required: {
                        email: 'Email is required',
                        password: 'Password is required'
                    }
                }
            },
            response: {
                200: userResponse
            }
        },
        handler: async (request, reply) => {
            const user = await loginUser(request.body.email, request.body.password,
                server.env.PASSWORD_HASH_SALT);

            if (!user) {
                throw server.httpErrors.badRequest('Username or password is incorrect.');
            }

            request.session.set('user', user);

            reply.send(user);
        }
    });

    server.route({
        method: 'GET',
        url: '/api/logout',
        schema: {
            response: {
                204: { type: 'null' }
            }
        },
        handler: async (request, reply) => {
            request.session.delete();

            reply.status(204).send(null);
        }
    });

    server.route({
        method: 'GET',
        url: '/api/scheduled-http-requests',
        schema: {
            response: {
                200: scheduleHttpRequestGetAllReponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            const tasks = await getTasks();

            reply.send(tasks);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'GET',
        url: '/api/scheduled-http-requests/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id']
            },
            response: {
                200: scheduleHttpRequestGetOneReponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            const task = await getTaskById(request.params.id);

            if (!task) {
                throw server.httpErrors.notFound();
            }

            reply.send(task);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'DELETE',
        url: '/api/scheduled-http-requests/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id']
            },
            response: {
                204: {
                    type: 'null'
                }
            }
        },
        preValidation: isAdmin(server),
        handler: async (request, reply) => {
            const tasksDeleted = await deleteTaskById(request.params.id);

            if (tasksDeleted < 1) {
                throw server.httpErrors.notFound();
            }

            reply.status(204).send();
        }
    });

    server.route({
        method: 'GET',
        url: '/api/users',
        schema: {
            response: {
                200: {
                    type: 'array',
                    items: {
                        ...userResponse
                    }
                }
            }
        },
        preValidation: isAdmin(server),
        handler: async (request, reply) => {
            const users = await getUsers();

            reply.send(users);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'GET',
        url: '/api/users/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id']
            },
            response: {
                200: userResponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            let user: ServerUtils.UsersTable;
            if (request.session.get('user').isAdmin) {
                user = await getUserById(request.params.id);
            } else {
                user = await getUserById(request.session.get('user').id);
            }

            reply.send(user);
        }
    });

    server.route<{ Body: ServerUtils.CreateUserParams }>({
        method: 'POST',
        url: '/api/users',
        schema: {
            body: {
                ...userRequest,
                required: ['email', 'password'],
                errorMessage: {
                    required: {
                        email: 'Email is required',
                        password: 'Password is required'
                    }
                }
            },
            response: {
                200: userResponse
            }
        },
        preValidation: isAdmin(server),
        handler: async (request, reply) => {
            let user: ServerUtils.UsersTable;

            if (request.session.get('user').isAdmin) {
                const userId = await createUser(request.body, server.env);

                user = await getUserById(userId[0]);
            } else {
                throw server.httpErrors.forbidden('You are not allowed to do that action');
            }

            reply.send(user);
        }
    });

    server.route<{ Params: { id: number }, Body: ServerUtils.UsersTable }>({
        method: 'PATCH',
        url: '/api/users/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id'],
            },
            body: {
                ...userRequest,
                required: ['email'],
                errorMessage: {
                    required: {
                        email: 'Email is required',
                    }
                }
            },
            response: {
                200: userResponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            let user: ServerUtils.UsersTable;

            if (request.session.get('user').isAdmin) {
                await updateUserById(request.params.id, request.body);
                user = await getUserById(request.params.id);
            } else {
                await updateUserById(request.params.id, { ...request.body, isAdmin: null });
                user = await getUserById(request.session.get('user').id);
            }
            reply.send(user);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'DELETE',
        url: '/api/users/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id'],
            },
            response: {
                204: { type: 'null' }
            }
        },
        preValidation: isAdmin(server),
        handler: async (request, reply) => {
            if (request.params.id === request.session.get('user').id) {
                throw server.httpErrors.conflict('You cannot delete your own user');
            }
            await deleteUserById(request.params.id);

            reply.status(204).send(null);
        }
    });

    server.route({
        method: 'GET',
        url: '/api/short-urls',
        schema: {
            response: {
                200: {
                    type: 'array',
                    items: {
                        ...shortUrlsResponse
                    }
                }
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            reply.send(await getShortUrls());
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'GET',
        url: '/api/short-urls/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id'],
            },
            response: {
                200: shortUrlsResponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            reply.send(await getShortUrlById(request.params.id));
        }
    });

    server.route({
        method: 'POST',
        url: '/api/short-urls',
        schema: {
            body: shortUrlsRequest,
            response: {
                200: shortUrlsResponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: createShortUrlHandler(server)
    });

    server.route<{ Params: { id: number }, Body: ServerUtils.ShortUrlsTable }>({
        method: 'PATCH',
        url: '/api/short-urls/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id'],
            },
            body: shortUrlsRequest,
            response: {
                200: shortUrlsResponse
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            await updateShortUrlById(request.params.id, request.body);

            const shortUrl = await getShortUrlById(request.params.id);

            reply.send(shortUrl);
        }
    });

    server.route<{ Params: { id: number } }>({
        method: 'DELETE',
        url: '/api/short-urls/:id',
        schema: {
            params: {
                type: 'object',
                properties: {
                    id: { type: 'number' }
                },
                required: ['id'],
            },
            response: {
                204: { type: 'null' }
            }
        },
        preValidation: isLoggedIn(server),
        handler: async (request, reply) => {
            await deleteShortUrlkById(request.params.id);

            reply.status(204).send(null);
        }
    });

    done();
}
