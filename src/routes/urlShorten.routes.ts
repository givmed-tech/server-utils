import { FastifyInstance, FastifyServerOptions } from 'fastify';
import { deleteShortUrlkById, getShortUrlByPath, getShortUrlById } from '../utils/db.util';
import { apiKeyValidator, createShortUrlHandler } from '../utils/core.util';
import { shortUrlsRequest, shortUrlsResponse } from '../schemas';

export default function attachUrlShortenRoutes(server: FastifyInstance, opts: FastifyServerOptions, done: () => void): void {
    server.route<{ Body: ServerUtils.ShortenUrlRequestPostData }>({
        method: 'POST',
        url: '/shorten-url',
        schema: {
            body: shortUrlsRequest,
            response: {
                200: shortUrlsResponse
            }
        },
        preValidation: apiKeyValidator(server),
        handler: createShortUrlHandler(server)
    });

    server.route<{ Body: { id: number } }>({
        method: 'DELETE',
        url: '/shorten-url',
        schema: {
            body: {
                type: 'object',
                properties: {
                    id: { type: 'number' },
                },
                required: ['id']
            },
            response: {
                204: {
                    type: 'null'
                }
            }
        },
        preValidation: apiKeyValidator(server),
        handler: async (request, reply) => {
            const shortUrlDeleted = await deleteShortUrlkById(request.body.id);

            if (shortUrlDeleted > 0) {
                return reply.code(204).send();
            } else {
                throw server.httpErrors.notFound('Failed to find short url to delete');
            }
        }
    });

    // MUST BE THE LAST ROUTE REGISTERED
    server.route<{ Params: { shortUrlParam: string | number; } }>({
        method: 'GET',
        url: '/:shortUrlParam',
        schema: {
            params: {
                shortUrlParam: { type: ['string', 'number'] }
            },
            response: {
                303: {
                    type: 'null'
                }
            }
        },
        handler: async (request, reply) => {
            const { shortUrlParam } = request.params;
            let shortUrl;

            if (isNaN(Number(shortUrlParam)) && typeof shortUrlParam === 'string') {
                shortUrl = await getShortUrlByPath(shortUrlParam);
            } else {
                shortUrl = await getShortUrlById(Number(shortUrlParam));
            }

            if (shortUrl) {
                return reply.code(303).redirect(shortUrl.originalUrl);
            } else {
                throw server.httpErrors.notFound();
            }
        }
    });

    done();
}
