import server from './server';
import { asyncServerReady } from './utils/core.util';

asyncServerReady(server).then(() => {
    server.listen({
        port: Number(server.env.PORT),
        host: '0.0.0.0',
    }, error => {
        if (error) {
            console.error(error);
            throw error;
        }

        console.info(`Server listening on ${server.env.PORT}`);
    });
}).catch(error => {
    console.error(error);
});
