import fastify from 'fastify';
import fastifyEnv from '@fastify/env';
import fastifyStatic from '@fastify/static';
import fastifySensible from '@fastify/sensible';
import fastifyHelmet from '@fastify/helmet';
import fastifyCompress from '@fastify/compress';
import fastifySecureSession from '@fastify/secure-session';
import ajvErrors from 'ajv-errors';
import path from 'path';
import attachScheduleHttpTaskRoutes  from './routes/scheduleHttpRequest.routes';
import attachUrlShortenRoutes from './routes/urlShorten.routes';
import attachDashboardRoutes from './routes/dashboard.routes';
import { env } from './schemas';

const isProd = process.env.NODE_ENV === 'production';

const server = fastify({
    logger: !isProd && !process.env.TEST_DB,
    ajv: {
        customOptions: {
            allErrors: true,
            allowUnionTypes: true
        },
        plugins: [
            ajvErrors
        ],
    }
});

server.addHook('onRoute', (routeOptions) => {
    if (Array.isArray(server.routes)) {
        server.routes.push(routeOptions.path);
    } else {
        server.routes = [
            routeOptions.path
        ];
    }
});

server.register(fastifyEnv, {
    confKey: 'env',
    schema: env,
});
server.register(fastifyCompress, { encodings: ['deflate', 'gzip'] });
server.register(fastifyHelmet, {
    contentSecurityPolicy: false,
});
server.register(fastifySecureSession, {
    secret: process.env.SECURE_SESSION_SECRET || env.properties.SECURE_SESSION_SECRET.default,
    salt: process.env.SECURE_SESSION_SALT || env.properties.SECURE_SESSION_SALT.default,
    cookie: {
        secure: isProd && !!(process.env.HTTPS || env.properties.HTTPS.default),
        httpOnly: true,
        sameSite: 'lax'
    }
});
server.register(fastifyStatic, {
    root: path.join(__dirname, '../public'),
    prefix: '/public/',
});
server.register(fastifySensible);

server.route<{ Body: ServerUtils.ShortenUrlRequestPostData }>({
    method: 'GET',
    url: '/server-utils',
    schema: {
        response: {
            200: {
                type: 'object',
                properties: {
                    dashboardPath: { type: 'string' },
                }
            }
        }
    },
    handler: (request, reply) => {
        reply.send({
            dashboardPath: process.env.ADMIN_DASHBOARD_URLPATH
                || env.properties.ADMIN_DASHBOARD_URLPATH.default
        });
    }
});
server.register(attachScheduleHttpTaskRoutes, { prefix: '/schedule' });
server.register(attachDashboardRoutes, { prefix: process.env.ADMIN_DASHBOARD_URLPATH
    || env.properties.ADMIN_DASHBOARD_URLPATH.default });
// IMPORTANT: This HAS to be the last route attach call. No routes should be attached after this call.
server.register(attachUrlShortenRoutes);

export default server;
