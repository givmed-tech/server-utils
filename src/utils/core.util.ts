import { FastifyRequest, FastifyReply, FastifyInstance } from 'fastify';
import { Server, IncomingMessage, ServerResponse } from 'http';
import crypto from 'crypto';
import { getActiveTasks, deleteTaskById, deleteExpiredTasks, getTaskById, updateTaskById, getUserByEmail, createUserIfNotExists, saveShortUrlInDB } from './db.util';
import axios from './axios.util';
import DBConfig from '../knexfile';

export function isProductionEnv(): boolean {
    return process.env.NODE_ENV === 'production';
}

export function getTimestampFromMs(ms: number): number | null {
    if (!ms || ms < 1) {
        return null;
    }
    return Date.now() + ms;
}

export async function completeHttpTask(params: ServerUtils.CompleteHttpTaskParams): Promise<void> {
    try {
        await DBConfig.db.transaction(async trx => {
            const task = await getTaskById(params.taskId, trx);
            const taskDeleted = await deleteTaskById(params.taskId, trx);

            if (task && task.id && taskDeleted > 0) {
                await axios({
                    url: params.requestUrl,
                    method: params.requestMethod,
                    params: {
                        ...(params.requestParams ?? {}),
                        uniqueExternalId: params.uniqueExternalId
                    },
                    data: {
                        ...(params.requestData ?? {}),
                        uniqueExternalId: params.uniqueExternalId
                    },
                    headers: {
                        ...(params.apiKey ? { Authorization: params.apiKey } : {})
                    },
                });
            }
        });
    } catch (error) {
        const task = await getTaskById(params.taskId);

        if (task && task.retryInterval && task.retryTimes > 0) {
            const retryTimestamp = Date.now() + task.retryInterval;

            await updateTaskById(params.taskId, {
                retryTimes: task.retryTimes - 1,
                timestamp: retryTimestamp,
                updated_at: Date.now()
            });

            scheduleTask({
                ...params,
                timestamp: retryTimestamp
            });
        } else {
            await deleteTaskById(params.taskId);
        }
    }
}

export function scheduleTask(params: ServerUtils.CompleteHttpTaskParams): void {
    const scheduleInMs = params.timestamp - Date.now();
    const maxSupportedTimeout = 2147483647;

    // if the ms to schedule is not supported by setTimeout,
    // don't schedule and retry at the max supported
    if (scheduleInMs > maxSupportedTimeout) {
        setTimeout(() => {
            scheduleTask({
                ...params,
                timestamp: getTimestampFromMs(scheduleInMs - maxSupportedTimeout)
            });
        }, maxSupportedTimeout);

        return;
    }

    setTimeout(async () => {
        try {
            await completeHttpTask(params);
        } catch (error) {
            // ignore
        }
    }, scheduleInMs);
}

export async function rescheduleActiveTasks(apiKey?: string): Promise<void> {
    const activeTasks = await getActiveTasks();

    for (const task of activeTasks) {
        scheduleTask({
            taskId: task.id,
            requestUrl: task.url,
            requestParams: task.params ? JSON.parse(task.params) : null,
            requestData: task.data ? JSON.parse(task.data) : null,
            requestMethod: task.method,
            uniqueExternalId: task.uniqueExternalId,
            timestamp: task.timestamp,
            apiKey
        });
    }
}

export async function asyncServerReady(server: FastifyInstance<Server, IncomingMessage,
ServerResponse>): Promise<FastifyInstance<Server, IncomingMessage, ServerResponse>> {
    return new Promise((resolve, reject) => {
        server.ready(error => {
            if (error) {
                reject(error);
            } else {
                Promise.all([
                    rescheduleActiveTasks(server.env.API_KEY),
                    deleteExpiredTasks(),
                    createUserIfNotExists({
                        email: server.env.ADMIN_USER_EMAIL,
                        password: server.env.ADMIN_USER_PASSWORD,
                        isAdmin: true,
                    }, server.env),
                ]).then(() => {
                    resolve(server);
                }).catch(error => {
                    reject(error);
                });
            }
        });
    });
}

export function isShortPathValid(path: string | undefined, existingRoutes: Array<string>): boolean {
    if (path && !path.startsWith('/')) {
        return false;
    }
    if (path && existingRoutes.includes(path)) {
        return false;
    }
    if (path && !isNaN(Number(path.replace('/','')))) {
        return false;
    }
    return true;
}

export function getShortUrl(publicUrl: string, id?: number, path?: string): string {
    let shortUrlPath = '';

    if (path && path.startsWith('/')) {
        shortUrlPath = path;
    } else if (path && !path.startsWith('/')){
        shortUrlPath = path.padStart(path.length + 1, '/');
    } else if (id) {
        shortUrlPath = `/${String(id)}`;
    }

    return publicUrl + shortUrlPath;
}

export function apiKeyValidator(server: FastifyInstance) {
    return async function(request: FastifyRequest): Promise<void> {
        const bearerToken = request.headers.authorization && request.headers.authorization.split('Bearer')[1]
            ? request.headers.authorization.split('Bearer')[1].trim() : null;

        if (server.env.API_KEY && bearerToken !== server.env.API_KEY) {
            throw server.httpErrors.unauthorized('Bearer token is not correct');
        }

        return;
    };
}

export async function hashPassword(password: string, salt: string): Promise<string> {
    return new Promise((resolve, reject) => {
        crypto.scrypt(password, salt, 64, (error, derivedKey) => {
            if (error) {
                reject(error);
            }

            resolve(derivedKey.toString());
        });
    });
}

export async function loginUser(email: string, password: string, salt: string): Promise<ServerUtils.UsersTable | null> {
    const user = await getUserByEmail(email);

    if (!user) {
        return null;
    }

    if (await hashPassword(password, salt) !== user.password) {
        return null;
    }

    return user;
}

export function createShortUrlHandler(server: FastifyInstance) {
    return async (request: FastifyRequest<{ Body: ServerUtils.ShortenUrlRequestPostData }>, reply: FastifyReply): Promise<FastifyReply> => {
        const { originalUrl, shortUrlPath } = request.body;

        if (!isShortPathValid(shortUrlPath, server.routes)) {
            throw server.httpErrors.badRequest('shortUrlPath is not valid');
        }

        let shortUrlId: number | null;

        try {
            shortUrlId = await saveShortUrlInDB(request.body);
        } catch (error) {
            server.assert.notEqual(error.code, 'SQLITE_CONSTRAINT', 400, error.message);
        }

        if (!shortUrlId) {
            throw server.httpErrors.internalServerError('Failed to insert short url into sqlite');
        }

        return reply.send({
            id: shortUrlId,
            originalUrl,
            ...(shortUrlPath ? { shortUrlPath } : {}),
            shortUrl: getShortUrl(server.env.PUBLIC_URL, shortUrlId, shortUrlPath)
        });
    };
}
