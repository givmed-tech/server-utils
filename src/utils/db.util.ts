import { Knex } from 'knex';
import DBConfig from '../knexfile';
import { hashPassword } from './core.util';

function _getRetrySettings(taskData: ServerUtils.ScheduleHttpRequestPostData,
    env: ServerUtils.FastifyEnv): { retryInterval: number; retryTimes: number } | Record<string, unknown> {

    if (taskData.retryInterval && taskData.retryTimes) {
        return {
            retryInterval: taskData.retryInterval,
            retryTimes: taskData.retryTimes
        };
    } else if (env.RETRY_INTERVAL && env.RETRY_TIMES) {
        return {
            retryInterval: env.RETRY_INTERVAL,
            retryTimes: env.RETRY_TIMES
        };
    } else {
        return {};
    }
}

// SCHEDULE HTTP REQUESTS TABLE
export async function saveTaskInDB(taskData: ServerUtils.ScheduleHttpRequestPostData,
    env: ServerUtils.FastifyEnv): Promise<Array<number>> {

    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .insert({
            timestamp: taskData.timestamp ?? Date.now() + taskData.ms,
            url: taskData.requestUrl,
            method: taskData.requestMethod,
            params: JSON.stringify(taskData.requestParams),
            data: JSON.stringify(taskData.requestData),
            uniqueExternalId: taskData.uniqueExternalId || null,
            rescheduleIfExpired: taskData.rescheduleIfExpired || false,
            created_at: Date.now(),
            updated_at: Date.now(),
            ..._getRetrySettings(taskData, env)
        });
}

/**
 * Active tasks are considered the ones that are not yet expired or the ones with rescheduleIfExpired === true
 */
export async function getActiveTasks(): Promise<Array<ServerUtils.HttpTasksTable>> {
    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('timestamp', '>=', Date.now()).orWhere('rescheduleIfExpired', '=', 1);
}

export async function getTasks(): Promise<Array<ServerUtils.HttpTasksTable>> {
    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .select('*');
}

export async function getTaskByUniqueExternalId(id: string, trx?: Knex.Transaction): Promise<ServerUtils.HttpTasksTable | undefined> {
    let query = DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('uniqueExternalId', id).select('*');

    if (trx) {
        query = query.transacting(trx);
    }

    return (await query)[0];
}

export async function getTaskById(id: number, trx?: Knex.Transaction): Promise<ServerUtils.HttpTasksTable> {
    let query = DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', id).select('*');

    if (trx) {
        query = query.transacting(trx);
    }

    return (await query)[0];
}

/**
 * Expired tasks are the ones that are expired and have rescheduleIfExpired set to null or false
 */
export async function deleteExpiredTasks(): Promise<number> {
    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('timestamp', '<', Date.now()).andWhere(function () {
            this.where('rescheduleIfExpired', '<>', 1).orWhereNull('rescheduleIfExpired');
        }).del();
}

export async function deleteTaskById(id: number, trx?: Knex.Transaction): Promise<number> {
    let query = DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', id).del();

    if (trx) {
        query = query.transacting(trx);
    }

    return await query;
}

export async function deleteTaskByUniqueExternalId(uniqueExternalId: string): Promise<number> {
    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('uniqueExternalId', uniqueExternalId).del();
}

export async function updateTaskById(id: number, taskData: ServerUtils.UpdateTaskById): Promise<void> {
    return await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', id).update(taskData);
}

// SHORT URLS TABLE
export async function saveShortUrlInDB(shortUrlData: ServerUtils.ShortenUrlRequestPostData): Promise<number> {
    return (await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls)
        .insert({
            originalUrl: shortUrlData.originalUrl,
            shortUrlPath: shortUrlData.shortUrlPath ?? null,
        }))[0];
}

export async function getShortUrls(): Promise<ServerUtils.ShortUrlsTable[]> {
    return await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).select('*');
}

export async function getShortUrlById(id: number): Promise<ServerUtils.ShortUrlsTable> {
    return (await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls)
        .where('id', id).select('*'))[0];
}

export async function getShortUrlByPath(path: string): Promise<ServerUtils.ShortUrlsTable> {
    return (await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls)
        .where('shortUrlPath', path.startsWith('/') ? path : `/${path}`).select('*'))[0];
}

export async function updateShortUrlById(id: number, shortUrlData: ServerUtils.ShortUrlsTable): Promise<void> {
    return await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls)
        .where('id', id).update({
            originalUrl: shortUrlData.originalUrl,
            shortUrlPath: shortUrlData.shortUrlPath,
        });
}

export async function deleteShortUrlkById(id: number): Promise<number> {
    return await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls)
        .where('id', id).del();
}

// USER TABLE
export async function getUserByEmail(email: string): Promise<ServerUtils.UsersTable> {
    return (await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .where('email', email).select('*'))[0];
}

export async function getUserById(id: number): Promise<ServerUtils.UsersTable> {
    return (await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .where('id', id).select('*'))[0];
}

export async function getUsers(): Promise<ServerUtils.UsersTable[]> {
    return await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .select('*');
}

export async function updateUserById(id: number, userData: ServerUtils.UsersTable): Promise<void> {
    return await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .where('id', id).update({
            email: userData.email,
            firstName: userData.firstName,
            lastName: userData.lastName,
            ...(userData.isAdmin !== null ? { isAdmin: userData.isAdmin } : {}),
        });
}

export async function createUser(userData: ServerUtils.CreateUserParams,
    env: ServerUtils.FastifyEnv): Promise<Array<number>> {

    return await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .insert({
            email: userData.email,
            password: await hashPassword(userData.password, env.PASSWORD_HASH_SALT),
            firstName: userData.firstName || null,
            lastName: userData.lastName || null,
            avatar: userData.avatar || null,
            isAdmin: userData.isAdmin || false,
        });
}

export async function createUserIfNotExists(userData: ServerUtils.CreateUserParams,
    env: ServerUtils.FastifyEnv): Promise<Array<number | null>> {

    if (await getUserByEmail(userData.email)) {
        return null;
    }

    return await createUser(userData, env);
}

export async function deleteUserById(id: number): Promise<number> {
    return await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users)
        .where('id', id).del();
}
