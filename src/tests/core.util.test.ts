process.env.TEST_DB = 'test-db0.sqlite';
import test from 'ava';
import server from '../server';
import { getTimestampFromMs, completeHttpTask, asyncServerReady, isShortPathValid, getShortUrl } from '../utils/core.util';
import { saveTaskInDB, getTaskById } from '../utils/db.util';
import DBConfig from '../knexfile';

test.before(async t => {
    await DBConfig.db.migrate.latest();

    process.env.RETRY_INTERVAL = '5000';
    process.env.RETRY_TIMES = '5';
    await asyncServerReady(server);
});

test('getTimestampFromMs returns the correct timestamp from milliseconds value', async t => {
    const ms = 3000;
    const timestamp = getTimestampFromMs(ms);
    t.true(timestamp <= Date.now() + ms + 5);
});

test('getTimestampFromMs returns null in case of invalid milliseconds value', async t => {
    const ms = -1;
    const timestamp = getTimestampFromMs(ms);
    t.is(timestamp, null);
});

test('getTimestampFromMs returns null in case of null/undefined milliseconds value', async t => {
    const ms1 = null;
    const ms2 = undefined;
    const timestamp1 = getTimestampFromMs(ms1);
    const timestamp2 = getTimestampFromMs(ms2);
    t.is(timestamp1, null);
    t.is(timestamp2, null);
});

test('completeHttpTask makes a successful http request', async t => {
    const baseTaskData = {
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
    };
    const taskId = await saveTaskInDB({
        ...baseTaskData,
        requestMethod: 'get',
        retryInterval: 3000,
        retryTimes: 2
    }, server.env);

    await completeHttpTask({
        ...baseTaskData,
        taskId: taskId[0],
        requestMethod: 'get',
    });

    const task = await getTaskById(taskId[0]);

    t.is(task, undefined);
});

test('completeHttpTask retries failed http request with retry schedule params', async t => {
    const baseTaskData = {
        timestamp: Date.now() + 15000,
        requestUrl: 'https://invalid.url',
    };
    const taskId = await saveTaskInDB({
        ...baseTaskData,
        requestMethod: 'get',
        retryInterval: 3000,
        retryTimes: 2
    }, server.env);

    await completeHttpTask({
        ...baseTaskData,
        taskId: taskId[0],
        requestMethod: 'get',
    });

    const task = await getTaskById(taskId[0]);

    t.is(task.retryInterval, 3000);
    t.is(task.retryTimes, 1);
});

test('completeHttpTask retries failed http request with retry env variables set', async t => {
    const baseTaskData = {
        timestamp: Date.now() + 15000,
        requestUrl: 'https://invalid.url',
    };
    const taskId = await saveTaskInDB({
        ...baseTaskData,
        requestMethod: 'get'
    }, server.env);

    await completeHttpTask({
        ...baseTaskData,
        taskId: taskId[0],
        requestMethod: 'get',
    });

    const task = await getTaskById(taskId[0]);

    t.is(task.retryInterval, 5000);
    t.is(task.retryTimes, 4);
});

test('isShortPathValid returns false on already used route', t => {
    t.false(isShortPathValid('/used-path', ['/used-path']));
    t.false(isShortPathValid('/used', ['/used-path', '/used']));
});

test('isShortPathValid returns false for path that does not start with /', t => {
    t.false(isShortPathValid('unused-path', ['/used-path']));
});

test('isShortPathValid returns false for path that is an integer', t => {
    t.false(isShortPathValid('/12', ['/used-path']));
});

test('isShortPathValid returns true for valid path', t => {
    t.true(isShortPathValid('/unused-path', ['/used-path']));
});

test('isShortPathValid returns false for undefined/null path', t => {
    t.true(isShortPathValid(undefined, ['/used-path']));
});

test('getShortUrl correct url with undefined path', t => {
    const localhostDomain = 'http://localhost';

    t.is(getShortUrl(localhostDomain, 1, undefined), `${localhostDomain}/1`);
});

test('getShortUrl correct url with path provided including forward slash', t => {
    const localhostDomain = 'http://localhost';

    t.is(getShortUrl(localhostDomain, 1, 'path'), `${localhostDomain}/path`);
});

test('getShortUrl correct url with path provided not including forward slash', t => {
    const localhostDomain = 'http://localhost';

    t.is(getShortUrl(localhostDomain, 1, '/path'), `${localhostDomain}/path`);
});
