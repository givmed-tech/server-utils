process.env.TEST_DB = 'test-db1.sqlite';
import test from 'ava';
import server from '../server';
import { asyncServerReady } from '../utils/core.util';
import {
    getActiveTasks,
    deleteExpiredTasks,
    saveTaskInDB,
    deleteTaskById,
    deleteTaskByUniqueExternalId,
    updateTaskById,
    getTaskById,
    saveShortUrlInDB,
    getShortUrlById,
    deleteShortUrlkById,
    getShortUrlByPath
} from '../utils/db.util';
import DBConfig from '../knexfile';

const activeTask = {
    timestamp: Date.now() + 15000,
    url: 'https://gitlab.com/givmed-tech/server-utils/',
    method: 'get' as const,
};
const expiredTask = {
    ...activeTask,
    timestamp: activeTask.timestamp - 20000
};
const expiredTaskReschedule = {
    ...activeTask,
    timestamp: activeTask.timestamp - 20000,
    rescheduleIfExpired: true
};

const shortUrl = {
    originalUrl: 'https://gitlab.com/givmed-tech/server-utils'
};

const shortUrl2 = {
    originalUrl: 'https://gitlab.com/givmed-tech/server-utils',
    shortUrlPath: '/serverutils'
};

test.before(async t => {
    await DBConfig.db.migrate.latest();
    await asyncServerReady(server);

    await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks).insert([
        activeTask,
        activeTask,
        expiredTask,
        expiredTask,
        expiredTaskReschedule
    ]);

    await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).insert([
        shortUrl,
        shortUrl2
    ]);
});

test.after(async t => {
    await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks).truncate();
    await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).truncate();
});

test.serial('deleteExpiredTasks deletes the expired tasks that have rescheduleIfExpired false or null', async t => {
    await deleteExpiredTasks();

    const expiredTasks = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('timestamp', '<', Date.now()).andWhere(function () {
            this.where('rescheduleIfExpired', '<>', 1).orWhereNull('rescheduleIfExpired');
        }).select();

    t.is(expiredTasks.length, 0);
});

test.serial('getActiveTasks gets remaining task that are still active', async t => {
    const tasks = await getActiveTasks();

    t.is(tasks.length, 3);
});

test.serial('saveTaskInDB saves a valid task with uniqueExternalId in the database', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
        uniqueExternalId: 'unique1'
    }, server.env);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', task[0]);

    t.is(savedTask[0].id, task[0]);
});

test.serial('saveTaskInDB does not save task with used uniqueExternalId', async t => {
    try {
        await saveTaskInDB({
            timestamp: Date.now() + 15000,
            requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
            requestMethod: 'get',
            uniqueExternalId: 'unique1'
        }, server.env);

        t.fail('task saved with already used uniqueExternalId');
    } catch (error) {
        t.pass();
    }
});

test('saveTaskInDB saves a valid task in the database', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', task[0]);

    t.is(savedTask[0].id, task[0]);
});

test('deleteTaskByUniqueExternalId deletes the task', async t => {
    const uniqueExternalId = '9283jd2398dj';
    await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
        uniqueExternalId
    }, server.env);

    const deleted = await deleteTaskByUniqueExternalId(uniqueExternalId);

    t.is(deleted, 1);
});

test('deleteTaskById deletes the task', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const deleted = await deleteTaskById(task[0]);

    t.is(deleted, 1);
});

test('getTaskById returns the task', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const getTask = await getTaskById(task[0]);

    t.is(getTask.id, task[0]);
});

test('updateTaskById updates the task data', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    await updateTaskById(task[0], { method: 'post' });

    const updatedTask = await getTaskById(task[0]);

    t.is(updatedTask.method, 'post');
});

test('saveShortUrlInDB saves a valid shortPath url', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
        shortUrlPath: '/server-utils'
    });

    const savedShortUrl = await getShortUrlById(shortUrlId);

    t.is(savedShortUrl.shortUrlPath, '/server-utils');
});

test('saveShortUrlInDB saves a valid url', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
    });

    const savedShortUrl = await getShortUrlById(shortUrlId);

    t.is(savedShortUrl.originalUrl, shortUrl.originalUrl);
});

test('saveShortUrlInDB fails on saving an existing shortPath url', async t => {
    await t.throwsAsync(saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
        shortUrlPath: '/serverutils'
    }));
});

test('deleteShortUrlkById deletes a short url', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
    });

    const deletedShortUrl = await deleteShortUrlkById(shortUrlId);

    t.is(deletedShortUrl, 1);
});

test('getShortUrlById gets the shortUrl', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
        shortUrlPath: '/server-utils-1'
    });
    const savedShortUrlById = await getShortUrlById(shortUrlId);

    t.is(savedShortUrlById.id, shortUrlId);
});

test('getShortUrlByPath gets the shortUrl', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: shortUrl.originalUrl,
        shortUrlPath: '/server-utils-2'
    });
    const [savedShortUrlByPathNoSlash, savedShortUrlByPathSlash] = await Promise.all([
        getShortUrlByPath('server-utils-2'),
        getShortUrlByPath('/server-utils-2')
    ]);

    t.is(savedShortUrlByPathNoSlash.id, shortUrlId);
    t.is(savedShortUrlByPathSlash.id, shortUrlId);
});
