process.env.TEST_DB = 'test-db4.sqlite';
import test from 'ava';
import server from '../../server';
import { asyncServerReady } from '../../utils/core.util';
import DBConfig from '../../knexfile';
import { getUserById } from '../../utils/db.util';

const adminUserEmail = 'admin@admin.com';
const activeTask = {
    timestamp: Date.now() + 15000,
    url: 'https://gitlab.com/givmed-tech/server-utils/',
    method: 'get' as const,
};
let testUser = {
    email: 'test@test.com',
    password: 'passw0rd',
    firstName: 'first',
    lastName: 'last',
    isAdmin: true,
};
let adminUserCookies = null;

const shortUrl = {
    originalUrl: 'https://gitlab.com/givmed-tech/server-utils',
    shortUrlPath: '/serverutilsv1'
};

const shortUrl2 = {
    originalUrl: 'https://gitlab.com/givmed-tech/server-utils',
    shortUrlPath: '/serverutilsv2'
};

test.before(async t => {
    await DBConfig.db.migrate.latest();
    await asyncServerReady(server);

    await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks).insert([
        activeTask,
        activeTask,
    ]);

    await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).insert([
        shortUrl,
        shortUrl2
    ]);
});

test.after(async t => {
    await DBConfig.db<ServerUtils.UsersTable>(DBConfig.tableNames.users).truncate();
    await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).truncate();
    await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks).truncate();
});

test.serial('POST /dashboard/api/login successfully logs in user', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/dashboard/api/login',
        payload: {
            email: adminUserEmail,
            password: 'passw0rd',
        },
    });

    const loggedInUser = JSON.parse(response.payload);

    adminUserCookies = response.cookies;

    t.not(adminUserCookies, null);
    t.is(loggedInUser.email, adminUserEmail);
});

test.serial('POST /dashboard/api/users successfully creates a user', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/dashboard/api/users',
        payload: testUser,
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const user: ServerUtils.UsersTable = JSON.parse(response.payload);

    testUser = {
        ...testUser,
        ...user,
    };

    const createdUser = await getUserById(user.id);

    t.is(user.email, testUser.email);
    t.is(user.firstName, testUser.firstName);
    t.is(user.lastName, testUser.lastName);
    t.is(createdUser.isAdmin, testUser.isAdmin);
});

test('GET /dashboard/api/users successfully fetches users', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/users',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const users: ServerUtils.BaseUser[] = JSON.parse(response.payload);

    t.is(users[0].email, adminUserEmail);
});

test('GET /dashboard/api/users/:id successfully fetches the user', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/users/1',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const user: ServerUtils.BaseUser = JSON.parse(response.payload);

    t.is(user.email, adminUserEmail);
});

test('PATCH /dashboard/api/users/:id successfully updates a user', async t => {
    const updatedLastName = 'updatedLast';

    const response = await server.inject({
        method: 'PATCH',
        url: '/dashboard/api/users/1',
        payload: {
            email: adminUserEmail,
            lastName: updatedLastName
        },
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const user: ServerUtils.UsersTable = JSON.parse(response.payload);

    t.is(user.lastName, updatedLastName);
});

test('DELETE /dashboard/api/users/:id successfully deletes a user', async t => {
    const response = await server.inject({
        method: 'DELETE',
        url: '/dashboard/api/users/2',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    t.is(response.statusCode, 204);
});

test('GET /dashboard/api/scheduled-http-requests successfully gets requests', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/scheduled-http-requests',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const scheduledUrls: ServerUtils.HttpTasksTable[] = JSON.parse(response.payload);

    t.is(scheduledUrls[0].url, activeTask.url);
});

test('GET /dashboard/api/scheduled-http-requests/:id successfully gets a request', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/scheduled-http-requests/1',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const scheduledUrl: ServerUtils.HttpTasksTable = JSON.parse(response.payload);

    t.is(scheduledUrl.url, activeTask.url);
});

test('DELETE /dashboard/api/scheduled-http-requests/:id successfully deletes a request', async t => {
    const response = await server.inject({
        method: 'DELETE',
        url: '/dashboard/api/scheduled-http-requests/2',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    t.is(response.statusCode, 204);
});

test('GET /dashboard/api/short-urls successfully gets short urls', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/short-urls',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const shortUrls: ServerUtils.ShortUrlsTable[] = JSON.parse(response.payload);

    t.is(shortUrls[0].originalUrl, shortUrl.originalUrl);
});

test('GET /dashboard/api/short-urls/:id successfully gets a short url', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/dashboard/api/short-urls/1',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    const shortUrlResponse: ServerUtils.ShortUrlsTable = JSON.parse(response.payload);

    t.is(shortUrlResponse.originalUrl, shortUrl.originalUrl);
});

test('POST /dashboard/api/short-urls/:id successfully creates a short url', async t => {
    const newShortUrlPath = '/serverutilsv3';
    const response = await server.inject({
        method: 'POST',
        url: '/dashboard/api/short-urls',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils',
            shortUrlPath: newShortUrlPath,
        },
    });

    const shortUrlResponse: ServerUtils.ShortUrlsTable = JSON.parse(response.payload);

    t.is(shortUrlResponse.shortUrlPath, newShortUrlPath);
});

test('PATCH /dashboard/api/short-urls/:id successfully updates a short url', async t => {
    const newShortUrlPath = '/serverutilsv4';

    const response = await server.inject({
        method: 'PATCH',
        url: '/dashboard/api/short-urls/1',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils',
            shortUrlPath: newShortUrlPath
        },
    });

    const shortUrlResponse: ServerUtils.ShortUrlsTable = JSON.parse(response.payload);

    t.is(shortUrlResponse.shortUrlPath, newShortUrlPath);
});

test('DELETE /dashboard/api/short-urls/:id successfully deletes a short url', async t => {
    const response = await server.inject({
        method: 'DELETE',
        url: '/dashboard/api/short-urls/2',
        cookies: {
            session: adminUserCookies.find(cookie => cookie.name === 'session').value
        },
    });

    t.is(response.statusCode, 204);
});
