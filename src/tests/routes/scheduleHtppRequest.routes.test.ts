process.env.TEST_DB = 'test-db2.sqlite';
import test from 'ava';
import server from '../../server';
import { asyncServerReady } from '../../utils/core.util';
import { saveTaskInDB } from '../../utils/db.util';
import DBConfig from '../../knexfile';

const activeTask = {
    timestamp: Date.now() + 15000,
    url: 'https://gitlab.com/givmed-tech/server-utils/',
    method: 'get' as const,
};
const apiKey = 'testapikey';

test.before(async t => {
    process.env.API_KEY = apiKey;
    await DBConfig.db.migrate.latest();
    await asyncServerReady(server);
});

test.after(async t => {
    await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks).truncate();
});

test.serial('POST /schedule/http-request successfully schedules a task with uniqueExternalId', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            ms: 10000,
            requestUrl: activeTask.url,
            requestData: { a: 'b' },
            requestMethod: activeTask.method,
            uniqueExternalId: 'unique2'
        }
    });
    const { taskId } = JSON.parse(response.payload);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', taskId);

    t.is(savedTask[0].id, taskId);
});

test.serial('POST /schedule/http-request returns 400 when a task with used uniqueExternalId is provided', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            ms: 10000,
            requestUrl: activeTask.url,
            requestData: { a: 'b' },
            requestMethod: activeTask.method,
            uniqueExternalId: 'unique2'
        }
    });

    t.is(response.statusCode, 400);
});

test('POST /schedule/http-request successfully schedules a task', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            ms: 10000,
            requestUrl: activeTask.url,
            requestData: { a: 'b' },
            requestMethod: activeTask.method,
        }
    });
    const { taskId } = JSON.parse(response.payload);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', taskId);

    t.is(savedTask[0].id, taskId);
});

test('DELETE /schedule/http-request successfully deletes a task by id', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const response = await server.inject({
        method: 'DELETE',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            id: task[0],
        }
    });

    t.is(response.statusCode, 204);
});

test('DELETE /schedule/http-request successfully deletes a task by uniqueExternalId', async t => {
    const uniqueExternalId = '293hd8237h';
    await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
        uniqueExternalId: uniqueExternalId
    }, server.env);

    const response = await server.inject({
        method: 'DELETE',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            uniqueExternalId: uniqueExternalId,
        }
    });

    t.is(response.statusCode, 204);
});

test('POST /schedule/http-request successfully schedules a task with rescheduleIfExpired', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            ms: 10000,
            requestUrl: activeTask.url,
            requestData: { a: 'b' },
            requestMethod: activeTask.method,
            rescheduleIfExpired: true
        }
    });
    const { taskId } = JSON.parse(response.payload);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', taskId).select('id', 'rescheduleIfExpired');

    t.is(savedTask[0].id, taskId);
    t.is(savedTask[0].rescheduleIfExpired, true);
});

test('POST /schedule/http-request successfully schedules a task with retries set', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            ms: 10000,
            requestUrl: activeTask.url,
            requestData: { a: 'b' },
            requestMethod: activeTask.method,
            retryInterval: 5 * 60 * 1000, // 5 minutes
            retryTimes: 3
        }
    });
    const { taskId } = JSON.parse(response.payload);

    const savedTask = await DBConfig.db<ServerUtils.HttpTasksTable>(DBConfig.tableNames.httpTasks)
        .where('id', taskId).select('id', 'retryInterval', 'retryTimes');

    t.is(savedTask[0].id, taskId);
    t.is(savedTask[0].retryInterval, 5 * 60 * 1000);
    t.is(savedTask[0].retryTimes, 3);
});

test('GET /schedule/http-request successfully gets all tasks', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const response = await server.inject({
        method: 'GET',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
    });

    const tasks = JSON.parse(response.payload);

    t.true(tasks.some(tsk => tsk.id === task[0]));
});

test('GET /schedule/http-request successfully gets task by uniqueExternalId', async t => {
    const uniqueExternalId = 'uniqueExtID';

    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
        uniqueExternalId
    }, server.env);

    const response = await server.inject({
        method: 'GET',
        url: '/schedule/http-request',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        query: {
            uniqueExternalId
        }
    });

    const tasks = JSON.parse(response.payload);

    t.is(tasks[0].id, task[0]);
});

test('GET /schedule/http-request/:id successfully gets task by id', async t => {
    const task = await saveTaskInDB({
        timestamp: Date.now() + 15000,
        requestUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        requestMethod: 'get',
    }, server.env);

    const response = await server.inject({
        method: 'GET',
        url: `/schedule/http-request/${task[0]}`,
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
    });

    const taskResponse = JSON.parse(response.payload);

    t.is(taskResponse.id, task[0]);
});
