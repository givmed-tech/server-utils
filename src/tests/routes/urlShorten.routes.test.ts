process.env.TEST_DB = 'test-db3.sqlite';
import test from 'ava';
import server from '../../server';
import { asyncServerReady } from '../../utils/core.util';
import { saveShortUrlInDB } from '../../utils/db.util';
import DBConfig from '../../knexfile';

const apiKey = 'testapikey';

test.before(async t => {
    process.env.API_KEY = apiKey;
    await DBConfig.db.migrate.latest();
    await asyncServerReady(server);
});

test.after(async t => {
    await DBConfig.db<ServerUtils.ShortUrlsTable>(DBConfig.tableNames.shortUrls).truncate();
});

test.serial('POST /shorten-url successfully saves and returns a short url with shortUrlPath provided', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
            shortUrlPath: '/serverutils'
        }
    });

    const shortUrlData = JSON.parse(response.payload);

    t.is(shortUrlData.originalUrl, 'https://gitlab.com/givmed-tech/server-utils/');
    t.is(shortUrlData.shortUrlPath, '/serverutils');
    t.is(shortUrlData.shortUrl, `${server.env.PUBLIC_URL}/serverutils`);
});

test('POST /shorten-url successfully saves and returns a short url', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        }
    });

    const shortUrlData = JSON.parse(response.payload);

    t.is(shortUrlData.originalUrl, 'https://gitlab.com/givmed-tech/server-utils/');
    t.is(shortUrlData.shortUrlPath, undefined);

    t.true(/http:\/\/localhost\/\d+/.test(shortUrlData.shortUrl));
});

test('POST /shorten-url does not accept already saved url with same shortUrlPath', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
            shortUrlPath: '/serverutils'
        }
    });

    t.is(response.statusCode, 400);
});

test('POST /shorten-url does not accept url with shortUrlPath that is used as a server route', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
            shortUrlPath: '/schedule/http-request'
        }
    });

    t.is(response.statusCode, 400);
});

test('POST /shorten-url does not accept url with shortUrlPath that does not start with /', async t => {
    const response = await server.inject({
        method: 'POST',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
            shortUrlPath: 'serverutils'
        }
    });

    t.is(response.statusCode, 400);
});

test('DELETE /shorten-url deletes a stored short url by id', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
    });

    const response = await server.inject({
        method: 'DELETE',
        url: '/shorten-url',
        headers: {
            'authorization': `Bearer ${apiKey}`
        },
        payload: {
            id: shortUrlId
        }
    });

    t.is(response.statusCode, 204);
});

test('GET /:shortUrlPath redirects to the correct url if found (path string)', async t => {
    await saveShortUrlInDB({
        originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
        shortUrlPath: '/short-url-1'
    });

    const response = await server.inject({
        method: 'GET',
        url: '/short-url-1',
    });

    t.is(response.statusCode, 303);
    t.is(response.headers.location, 'https://gitlab.com/givmed-tech/server-utils/');
});

test('GET /:shortUrlPath redirects to the correct url if found (path number/id)', async t => {
    const shortUrlId = await saveShortUrlInDB({
        originalUrl: 'https://gitlab.com/givmed-tech/server-utils/',
    });

    const response = await server.inject({
        method: 'GET',
        url: `/${shortUrlId}`,
    });

    t.is(response.statusCode, 303);
    t.is(response.headers.location, 'https://gitlab.com/givmed-tech/server-utils/');
});

test('GET /:shortUrlPath returns 404 if short url not found', async t => {
    const response = await server.inject({
        method: 'GET',
        url: '/not-found-short-url',
    });

    t.is(response.statusCode, 404);
});
