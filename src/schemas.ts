export const env = {
    type: 'object',
    required: ['PORT', 'NODE_ENV'],
    properties: {
        TEST_DB: {
            type: ['string', 'null'],
            default: null
        },
        API_KEY: {
            type: ['string', 'null'],
            default: null
        },
        PORT: {
            type: 'number',
            default: 4545
        },
        NODE_ENV: {
            type: 'string',
            default: 'development'
        },
        RETRY_INTERVAL: {
            type: ['number', 'null'],
            default: null
        },
        RETRY_TIMES: {
            type: 'number',
            default: 0
        },
        PUBLIC_URL: {
            type: 'string',
            default: 'http://localhost'
        },
        SECURE_SESSION_SECRET: {
            type: 'string',
            default: 'averylogphrasebiggerthanthirtytwochars'
        },
        SECURE_SESSION_SALT: {
            type: 'string',
            default: 'mq9hDxBVDbspDR6n'
        },
        PASSWORD_HASH_SALT: {
            type: 'string',
            default: 'mq9hDxBVDbspDR6n'
        },
        ADMIN_USER_EMAIL: {
            type: 'string',
            default: 'admin@admin.com'
        },
        ADMIN_USER_PASSWORD: {
            type: 'string',
            default: 'passw0rd'
        },
        ADMIN_DASHBOARD_URLPATH: {
            type: 'string',
            default: '/dashboard'
        },
        HTTPS: {
            type: 'boolean',
            default: false
        }
    }
};

export const scheduleHttpRequestPostBody = {
    type: 'object',
    properties: {
        ms: { type: ['integer', 'null'] },
        timestamp: { type: ['integer', 'null'] },
        requestUrl: { type: 'string' },
        requestData: { type: 'object' },
        requestParams: { type: 'object' },
        requestMethod: {
            type: 'string',
            enum: ['get', 'post', 'put', 'delete'],
        },
        uniqueExternalId: { type: 'string' },
        retryInterval: { type: 'integer' },
        retryTimes: { type: 'integer' }
    },
    required: ['requestUrl', 'requestMethod']
};

export const scheduleHttpRequestDeleteBody = {
    type: 'object',
    properties: {
        id: { type: 'number' },
        uniqueExternalId: { type: 'string' }
    },
};

export const scheduleHttpRequestGetOneReponse = {
    type: 'object',
    properties: {
        id: { type: 'number' },
        timestamp: { type: 'number' },
        url: { type: 'string' },
        method: { type: 'string',
            enum: ['get', 'post', 'put', 'delete'] },
        params: { type: 'string' },
        data: { type: 'string' },
        uniqueExternalId: { type: ['string', 'null'] },
        rescheduleIfExpired: { type: ['boolean', 'null'] },
        retryInterval: { type: ['number', 'null'] },
        retryTimes: { type: 'number' },
        created_at: { type: ['number', 'null'] },
        updated_at: { type: ['number', 'null'] },
    }
};

export const scheduleHttpRequestGetAllReponse = {
    type: 'array',
    items:{
        ...scheduleHttpRequestGetOneReponse
    }
};

export const minLength6 = 6;

export const userResponse = {
    type: 'object',
    properties: {
        id: { type: 'number' },
        email: {
            type: 'string',
            minLength: minLength6,
            errorMessage: {
                minLength: `Email has to be at least ${minLength6} characters long`
            }
        },
        firstName: { type: ['string', 'null'] },
        lastName: { type: ['string', 'null'] },
        avatar: { type: ['string', 'null'] },
        isAdmin: { type: 'boolean' },
    }
};

export const userRequest = {
    type: 'object',
    properties: {
        ...userResponse.properties,
        password: {
            type: ['string', 'null'],
            minLength: minLength6,
            errorMessage: {
                type: 'Password must be filled',
                minLength: `Password has to be at least ${minLength6} characters long`
            }
        },
    },
};

export const shortUrlsRequest = {
    type: 'object',
    properties: {
        originalUrl: { type: 'string' },
        shortUrlPath: { type: ['string', 'null'] }
    },
    required: ['originalUrl'],
    errorMessage: {
        required: {
            originalUrl: 'Original url is required',
        }
    }
};

export const shortUrlsResponse = {
    type: 'object',
    properties: {
        id: { type: 'number' },
        originalUrl: { type: 'string' },
        shortUrlPath: { type: 'string' },
        shortUrl: { type: ['string', 'null'] },
        created_at: { type: ['number', 'null'] },
        updated_at: { type: ['number', 'null'] },
    }
};
