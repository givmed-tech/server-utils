import { Knex } from 'knex';
import DBConfig from '../knexfile';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(DBConfig.tableNames.shortUrls, table => {
        table.increments('id').primary();
        table.string('originalUrl');
        table.string('shortUrlPath').index().nullable().defaultTo(null).unique();
        table.timestamps();
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(DBConfig.tableNames.shortUrls);
}

export const config = { transaction: false };
