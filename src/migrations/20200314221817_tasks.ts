import { Knex } from 'knex';
import DBConfig from '../knexfile';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(DBConfig.tableNames.httpTasks, table => {
        table.increments('id').primary();
        table.integer('timestamp').index();
        table.string('uniqueExternalId').index().nullable();
        table.string('url');
        table.string('method');
        table.string('params').nullable();
        table.string('data').nullable();
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(DBConfig.tableNames.httpTasks);
}

export const config = { transaction: false };
