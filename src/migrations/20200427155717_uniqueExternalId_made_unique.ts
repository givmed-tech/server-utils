import { Knex } from 'knex';
import DBConfig from '../knexfile';

const columnName = 'uniqueExternalId';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.unique([columnName]);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.dropUnique([columnName]);
    });
}
