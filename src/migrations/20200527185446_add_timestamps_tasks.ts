import { Knex } from 'knex';
import DBConfig from '../knexfile';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.timestamps();
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.dropTimestamps();
    });
}

export const config = { transaction: false };
