import { Knex } from 'knex';
import DBConfig from '../knexfile';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(DBConfig.tableNames.users, table => {
        table.increments('id').primary();
        table.string('email').index().unique();
        table.string('password');
        table.string('firstName').nullable();
        table.string('lastName').nullable();
        table.binary('avatar').nullable();
        table.boolean('isAdmin').nullable().defaultTo(false);
        table.timestamps(true, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable(DBConfig.tableNames.users);
}

export const config = { transaction: false };
