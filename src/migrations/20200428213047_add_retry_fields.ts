import { Knex } from 'knex';
import DBConfig from '../knexfile';

const retryIntervalColumnName = 'retryInterval';
const retryTimesColumnName = 'retryTimes';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.integer(retryIntervalColumnName).nullable().defaultTo(null);
        table.integer(retryTimesColumnName).defaultTo(0);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.dropColumn(retryIntervalColumnName);
        table.dropColumn(retryTimesColumnName);
    });
}
