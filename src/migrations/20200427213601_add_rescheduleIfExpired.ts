import { Knex } from 'knex';
import DBConfig from '../knexfile';

const columnName = 'rescheduleIfExpired';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.boolean(columnName).nullable().defaultTo(false);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.alterTable(DBConfig.tableNames.httpTasks, table => {
        table.dropColumn(columnName);
    });
}
