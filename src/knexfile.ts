import knex, { Knex } from 'knex';

function convertTinyIntToBoolean(object) {
    if (object && typeof object['rescheduleIfExpired'] === 'number') {
        object['rescheduleIfExpired'] = object['rescheduleIfExpired'] === 1;
    } else if (object && typeof object['isAdmin'] === 'number') {
        object['isAdmin'] = object['isAdmin'] === 1;
    }
    return object;
}

const prodKnexConfig = {
    client: 'sqlite3',
    useNullAsDefault: true,
    debug: false,
    connection: {
        filename: `${__dirname}/../db/db.sqlite`
    },
    migrations: {
        extension: 'ts',
        tableName: 'knex_migrations',
        directory: `${__dirname}/migrations`
    },
    postProcessResponse: (result, queryContext) => {
        if (Array.isArray(result)) {
            return result.map(row => convertTinyIntToBoolean(row));
        } else {
            return convertTinyIntToBoolean(result);
        }
    }
};

const knexConfig: {
    development: Knex.Config;
    production: Knex.Config;
} = {
    development: {
        ...prodKnexConfig,
        connection: { filename: `${__dirname}/../db/${process.env.TEST_DB || 'dev-db.sqlite'}` }
    },
    production: prodKnexConfig
};

const tableNames = {
    httpTasks: 'http_tasks',
    shortUrls: 'short_urls',
    users: 'users',
};

const db = knex(knexConfig[process.env.NODE_ENV || 'development']);

export = {
    ...knexConfig,
    tableNames,
    db
};
