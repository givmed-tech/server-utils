import './global.scss';
import App from './Dashboard.svelte';

const app = new App({
    target: document.body,
    props: {}
});

export default app;
