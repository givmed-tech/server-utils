/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { writable, derived } from 'svelte/store';
import localForage from 'localforage';
import routes from './routes';

let notifyTimeoutId;

function createPersistedStore(storeName, defaultValue = null) {
    const { subscribe, set, update } = writable(null);

    localForage.getItem(storeName).then(value => set(value || defaultValue))
        .catch(console.error);

    return {
        subscribe,
        update,
        set: valueToSet => {
            localForage.setItem(storeName, valueToSet).then(value => set(value))
                .catch(console.error);
        },
        clear: () => {
            localForage.removeItem(storeName).then(() => set(defaultValue))
                .catch(console.error);
        }
    };
}

export const user = createPersistedStore('user');
export const isLoggedIn = derived(user, $user => !!$user);

export const currentRoute = createPersistedStore('currentRoute', routes.scheduledTasks);

export const notificationMessage = writable(null);
export const notificationClass = writable('is-success');
export function notifyAndClear(notifyMessage, notifyClass, clearInMs = 5000) {
    if (notifyTimeoutId) {
        clearTimeout(notifyTimeoutId);
    }

    notificationMessage.set(notifyMessage);
    notificationClass.set(notifyClass);

    notifyTimeoutId = setTimeout(() => {
        notificationMessage.set(null);
        notificationClass.set('is-success');
    }, clearInMs);
}
