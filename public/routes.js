export default {
    scheduledTasks: 'scheduled-tasks',
    shortUrls: 'short-urls',
    users: 'users',
    profile: 'profile',
};
