/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import axios from 'axios';
import axiosRetry from 'axios-retry';
import { user, notifyAndClear } from './stores.js';

export function getCookie(cname) {
    const name = cname + '=';
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');

    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
}

export const axiosInstance = axios.create({
    timeout: 2000,
    headers: {
        'Content-Type': 'application/json',
    }
});

async function getAxiosBaseUrl() {
    const response = await axios.get('/server-utils');
    return response.data.dashboardPath;
}

axiosRetry(axiosInstance, {
    retries: 3
});

export async function bootstrap() {
    axiosInstance.defaults.baseURL = `${await getAxiosBaseUrl()}/api`;
}

export function handleError(error) {
    if (error.isAxiosError) {
        // log on dev mode only
        console.error(error.toJSON());

        if (error.response && error.response.status === 401) {
            user.set(null);
            notifyAndClear('You need to log in again.', 'is-info');
            return;
        } else if (error.response && error.response.data
            && error.response.data.message) {
            notifyAndClear(error.response.data.message, 'is-danger');
            return;
        }
    } else {
        // log on dev mode only
        console.error(error);
    }

    notifyAndClear('Something went wrong. Please try again.', 'is-danger');
}
