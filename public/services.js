/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { axiosInstance } from './utils.js';

export async function saveProfileData(profileData) {
    const response = await axiosInstance({
        url: `/users/${profileData.id}`,
        method: 'PATCH',
        data: profileData,
    });

    return response.data;
}
