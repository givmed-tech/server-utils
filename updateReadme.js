const fs = require('fs');
const { version } = require('./package.json');

const readmeTemplate = './readme.template.md';
const readmeFile = './README.md';

let data = fs.readFileSync(readmeTemplate, 'utf8');
data = data.replace(/<<version-tag>>/g, `v${version}`);

fs.writeFileSync(readmeFile, data);