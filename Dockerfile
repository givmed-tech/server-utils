FROM node:18

WORKDIR /app

COPY package*.json ./

RUN npm ci --only=production

COPY . .

ENV NODE_ENV=production
ENV PORT=4545

EXPOSE 4545

VOLUME ["/app/db", "/app/dist/migrations"]

CMD [ "npm", "run", "start:prod" ]