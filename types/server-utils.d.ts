declare namespace ServerUtils {
    interface FastifyEnv {
        PORT: number;
        NODE_ENV: string;
        API_KEY: string | null;
        RETRY_INTERVAL: number | null;
        RETRY_TIMES: number;
        PUBLIC_URL: string;
        SECURE_SESSION_SECRET: string;
        SECURE_SESSION_SALT: string;
        PASSWORD_HASH_SALT: string;
        ADMIN_USER_EMAIL: string;
        ADMIN_USER_PASSWORD: string;
    }
    interface BaseUser {
        email: string;
        firstName?: string;
        lastName?: string;
        avatar?: string;
        isAdmin?: boolean;
    }

    // fastify schema objects
    interface ScheduleHttpRequestPostData {
        ms?: number;
        timestamp?: number;
        requestUrl: string;
        requestMethod: 'get' | 'post' | 'put' | 'delete';
        requestParams?: Record<string, unknown>;
        requestData?: Record<string, unknown>;
        uniqueExternalId?: string;
        rescheduleIfExpired?: boolean;
        retryInterval?: number;
        retryTimes?: number;
    }
    interface ScheduleHttpRequestDeleteData {
        id?: number;
        uniqueExternalId?: string;
    }
    interface ShortenUrlRequestPostData {
        originalUrl: string;
        shortUrlPath?: string;
    }

    // knex tables
    interface HttpTasksTable {
        id: number;
        timestamp: number;
        url: string;
        method: 'get' | 'post' | 'put' | 'delete';
        params: string;
        data: string;
        uniqueExternalId?: string;
        rescheduleIfExpired: boolean | null;
        retryInterval: number | null;
        retryTimes: number;
        created_at?: number;
        updated_at?: number;
    }
    interface ShortUrlsTable {
        id: number;
        originalUrl: string;
        shortUrlPath: string | null;
        created_at: string;
        updated_at: string;
    }
    interface UsersTable {
        id: number;
        email: string;
        password: string;
        firstName?: string;
        lastName?: string;
        avatar?: string;
        isAdmin?: boolean;
    }
    interface UsersTable extends BaseUser {
        id: number;
        password: string;
    }

    // function params
    interface CompleteHttpTaskParams {
        taskId: number;
        requestUrl: string;
        requestMethod: 'get' | 'post' | 'put' | 'delete';
        requestParams?: Record<string, unknown>;
        requestData?: Record<string, unknown>;
        uniqueExternalId?: string;
        apiKey?: string;
        timestamp: number;
    }
    interface UpdateTaskById {
        timestamp?: number;
        url?: string;
        method?: 'get' | 'post' | 'put' | 'delete';
        params?: string;
        data?: string;
        uniqueExternalId?: string;
        rescheduleIfExpired?: boolean | null;
        retryInterval?: number | null;
        retryTimes?: number;
        created_at?: number;
        updated_at?: number;
    }
    interface CreateUserParams extends BaseUser {
        password: string;
    }
}
