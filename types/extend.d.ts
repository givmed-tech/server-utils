import { Server, IncomingMessage, ServerResponse } from 'http';
declare module 'fastify' {
    interface FastifyInstance {
        env: ServerUtils.FastifyEnv;
        routes?: Array<string>;
    }
}
