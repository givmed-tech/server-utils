# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.4.1] - 04-03-2023

### Added
- Upgraded packages and nodejs to v18.

## [2.4.0] - 27-11-2021

### Added
- There is no longer a requirement of max `2147483647 ms` for the scheduling.

## [2.3.2] - 29-10-2021

### Updated
- Updated packages and nodejs to latest `v16`.

## [2.3.0] - 17-05-2021

### Added
- Added endpoints to get scheduled tasks `GET /schedule/http-request` & `GET /schedule/http-request/:id`. (check `README.md`)
### Updated
- Updated packages and nodejs to latest `v14.17.0`.

## [2.2.5] - 31-12-2020

### Updated
- Updates packages

## [2.2.4] - 15-11-2020

### Updated
- Updates packages and nodejs version to 14

## [2.2.1] - 12-09-2020

### Fixed
- Fixes issues with secure session cookie, running on localhost, csrf token and chrome compression

## [2.2.0] - 11-09-2020

### Added
- Added the admin dashboard url path env variable and functionality `ADMIN_DASHBOARD_URLPATH`.
### Fixed
- Readme file documentation

## [2.1.0] - 09-09-2020

### Added
- Added the admin dashboard accessible at `<PUBLIC_URL>/dashboard`.

## [2.0.2] - 10-06-2020

### Fixed
- minor fixes and improvements plus restriction on schedule endpoint to fix errors

## [2.0.1] - 27-05-2020

### Fixed
- `POST` on shorten url endpoint doesn't allow passing an integer as `shortUrlPath` to avoid conficts with db ids.

## [2.0.0] - 24-05-2020

### Added
- Added url shortening functionality and endpoints.
### Changed
- `POST/DELETE /schedule/http-request` requires the `Authorization Bearer API_KEY` token header.

## [1.3.2] - 18-05-2020

### Changed
- Changed Dainar to givemed-tech in project.

## [1.3.1] - 18-05-2020

### Changed
- Changed git remote to givmed-tech.

## [1.3.0] - 04-05-2020

### Added
- Added retry on error logic. See README for how to enable.

## [1.2.0] - 28-04-2020

### Added
- Added `rescheduleIfExpired` option in the post schedule http request. Defaults to false.

## [1.1.2] - 27-04-2020

### Changed
- Made `uniqueExternalId` unique as a database constraint. Schedule endpoint returns 400 in case of used `uniqueExternalId`.

## [1.1.1] - 23-04-2020

### Initial version