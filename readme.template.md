[![Latest Release](https://gitlab.com/givmed-tech/server-utils/-/badges/release.svg)](https://gitlab.com/givmed-tech/server-utils/-/releases)
[![coverage report](https://gitlab.com/givmed-tech/server-utils/badges/main/coverage.svg)](https://gitlab.com/givmed-tech/server-utils/-/commits/main)
[![pipeline status](https://gitlab.com/givmed-tech/server-utils/badges/main/pipeline.svg)](https://gitlab.com/givmed-tech/server-utils/-/commits/main)

# Server utils
`server-utils` provides two main functionalities as of now:
* Scheduling http requests
* URL shortening

Scheduling http requests to an external system or back to your api/server. You can use it if you don't want to take care of the scheduling logic in your code.
<br>It also provides a url shortening utility which allows you to generate short urls used for sharing/tracking.
<br>The recommended way to run it is via the docker image provided.
<br>This is the code of the server-utils project which can be used to schedule tasks (http requests as of now). The server is implemented with the [fastify](https://www.fastify.io/) framework in [typescript](https://www.typescriptlang.org/). It uses [sqlite3](https://sqlite.org/index.html) with the [knexjs](https://knexjs.org/) query builder to save the tasks in order to be able to resume in case of an unexpected shutdown. The dashboard frontend is built with [svelte](https://svelte.dev/).

## <a name="using-server-utils">Using server-utils</a>
`server-utils` is provided as a docker image at the [server-utils registry](https://gitlab.com/givmed-tech/server-utils/container_registry).

### <a name="running-the-container">Running the container</a>
An example of running the container is:
```sh
docker run --name server-utils -p 4545:4545 -v db:/app/db -v migrations:/app/dist/migrations registry.gitlab.com/givmed-tech/server-utils:latest
```
* You can pass any of the environment variables listed [here](#env-variables) in the run command.
* You need to create a volume for sqlite and the migrations folder so if the container restarts, it can pick up the remaining scheduled tasks and re-schedule them.

### <a name="schedule-http-tasks">Schedule http tasks</a>
After you've got the container running you can schedule http tasks. Below are the http calls supported:
#### POST `/schedule/http-request`
##### Http body:
```json
{
    ms: { type: 'integer' }, // milliseconds for when it should make the http call
    timestamp: { type: 'integer' }, // or utc timestamp on when to make the http call
    requestUrl: { type: 'string' }, // url it should make the http call to
    requestData: { type: 'object' }, // data to send to the url specified
    requestParams: { type: 'object' }, // url query params to send to the url specified
    requestMethod: {
        type: 'string',
        enum: ['get', 'post', 'put', 'delete'],
    }, // http method to use when making the call
    uniqueExternalId: { type: 'string' } // optional, a unique (db constraint) id that will be passed back in the http call
    rescheduleIfExpired: { type: 'boolean' } // optional, if it should be rescheduled immediately in case it's expired and server stopped unexpectedly. Default: false. (it gets removed)
    retryInterval: { type: 'integer' } // optional, integer declaring the milliseconds between the retries in case of an error (requires `retryTimes` to be sent too)
    retryTimes: { type: 'integer' } // optional, integer declaring the number of retries in case of an error (requires `retryInterval` to be sent too)
}
```
You can either pass an `ms` value which is the number of milliseconds from now to make the http call or a utc `timestamp` which the datetime on when to make the http call.<br>
##### Response:
```json
{
    taskId: { type: 'number' }, // id of the task that is scheduled
    uniqueExternalId: { type: ['string', 'null'] } // uniqueExternalId if it was passed in the schedule call
}
```

#### GET `/schedule/http-request`
##### Query Params:
```json
{
    "uniqueExternalId": { "type": ["string", "null"] }, // optional
}
```
##### Response:
Maybe be an empty array if no tasks are currently scheduled
```json
[{
    "id": { "type": "number" },
    "timestamp": { "type": "number" },
    "url": { "type": "string" },
    "method": { "type": "string", "enum": ["get", "post", "put", "delete"] },
    "params": { "type": "string" },
    "data": { "type": "string" },
    "uniqueExternalId": { "type": ["string", "null"] },
    "rescheduleIfExpired": { "type": ["boolean", "null"] },
    "retryInterval": { "type": ["number", "null"] },
    "retryTimes": { "type": "number" },
    "created_at": { "type": ["number", "null"] },
    "updated_at": { "type": ["number", "null"] },
}]
```

#### GET `/schedule/http-request/:id`
##### Url Param:
```json
{
    "id": { "type": "number" }
}
```
##### Response:
Will respond with a `404` if the task is not found
```json
{
    "id": { "type": "number" },
    "timestamp": { "type": "number" },
    "url": { "type": "string" },
    "method": { "type": "string", "enum": ["get", "post", "put", "delete"] },
    "params": { "type": "string" },
    "data": { "type": "string" },
    "uniqueExternalId": { "type": ["string", "null"] },
    "rescheduleIfExpired": { "type": ["boolean", "null"] },
    "retryInterval": { "type": ["number", "null"] },
    "retryTimes": { "type": "number" },
    "created_at": { "type": ["number", "null"] },
    "updated_at": { "type": ["number", "null"] },
}
```

#### DELETE `/schedule/http-request`
##### Http body:
```json
{
    id: { type: 'number' }, // id of the task to delete
    uniqueExternalId: { type: 'string' } // or uniqueExternalId of the task to delete if one was provided
}
```

### <a name="shorten-url">URL shortener</a>
#### POST `/shorten-url`
##### Http body:
```json
{
    originalUrl: { type: 'string' },
    shortUrlPath: { type: ['string', 'null'] }
}
```
You can either pass a `shortUrlPath` value which will be the short url path or keep it empty and it will use the short url database id as the short path.<br>
##### Response:
```json
{
    id: { type: 'number' },
    originalUrl: { type: 'string' },
    shortUrlPath: { type: 'string' },
    shortUrl: { type: 'string' }
}
```
#### DELETE `/shorten-url`
##### Http body:
```json
{
    id: { type: 'number' }
}
```

#### Notes on scheduling
* If the system goes down for any reason and it restarts, by default it will only reschedule non expired (future) tasks. You can set the `rescheduleIfExpired` attribute to `true` to immediatly reschedule any expired tasks too on system start.
* There is no retry mechanism enabled by default. If you want to enable retries in case of error (either db issue or http request error) you can do with by setting either the environment variables or passing the retry params during the schedule call. If both are sent the schedule http request params will persist for that request.
* The `API_KEY` is used (if provided) as an authorization bearer token in the `POST` and `DELETE` calls to secure the endpoints.

#### Notes on url shortening
* The `PUBLIC_URL` env variable will be used to dynamically generate the short url returned on the `POST` call.
* The `API_KEY` is used (if provided) as an authorization bearer token in the `POST` and `DELETE` calls to secure the endpoints.

### <a name="admin-dashboard">Admin dashboard</a>
From version `2.1+` Server Utils expose their own dashboard at the `PUBLIC_URL/dashboard` route.
<br>In order to start using the dashboard you need to pass the `SECURE_SESSION_SECRET`, `SECURE_SESSION_SALT`, `PASSWORD_HASH_SALT`, `ADMIN_USER_EMAIL` and `ADMIN_USER_PASSWORD`. You can find more details on the environment variables at the environment variables section [here](#env-variables).
<br>The dashboard gives you the option to create more users, create short urls from the ui and monitor the scheduled http tasks.

## <a name="env-variables">ENV variables</a>
Environment variables for the docker container:
* `API_KEY` : `string`
<br> If set, the http requests scheduled are going to include the `API_KEY` in an `Authorization` header. It's also used as a required `Authorization Bearer` header for the `POST` and `DELETE` calls on the url shortening endpoints.
* `RETRY_INTERVAL` : `integer`
<br> If set, the retry mechanism is enabled on error. Set an integer declaring the milliseconds between the retries. (Requires `RETRY_TIMES` to be set)
* `RETRY_TIMES` : `integer`
<br> If set, the retry mechanism is enabled on error. Set the times it should retry each task in case of an error. (Requires `RETRY_INTERVAL` to be set)
* `PUBLIC_URL` : `string`
<br> It defines the public url that server-utils are hosted at. It needs to include the protocol and port (if applicable). For example: `https://example.com`. It defaults to `http://localhost`.
* `SECURE_SESSION_SECRET` : `string`
<br> 32 (minimum) character string used to encrypt the secure session cookie data.
* `SECURE_SESSION_SALT` : `string`
<br> 16 character string used to hash the secure session cookie data.
* `PASSWORD_HASH_SALT` : `string`
<br> 16 character string used to hash the password of the users in the db.
* `ADMIN_USER_EMAIL` : `string`
<br> The email of the admin user to log in to the dashboard.
* `ADMIN_USER_PASSWORD` : `string`
<br> The password for the admin user to log in to the dashboard (minimum 6 characters long).
* `ADMIN_DASHBOARD_URLPATH` : `string`
<br> The url path you want the main dashboard route to be exposed (defaults to `/dashboard` and is exposed at `<PUBLIC_URL>/dashboard`).
* `HTTPS` : `boolean` (defaults to `false`)
<br> Set this to true if server utils is served over https.

## Development notes
* No routes should be attached after the `attachUrlShortenRoutes` call since it uses a catch-all route to handle the short urls generated.